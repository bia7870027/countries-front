# *Where in the world?* app

![App preview](./preview.png)

## How to run

1. Make sure you're using Node 18+ on your machine
1. Install the dependencies
    ```bash
    npm i
    ```
1. Run the development server
    ```bash
    npm run dev
    ```
1. Visit [`http://localhost:3000`](http://localhost:3000)

## Project architecture

```
src/
├── app
│   ├── country
│   │   └── [slug]
│   │       ├── page.module.css
│   │       └── page.tsx        // Country page (/country/<slug>)
│   ├── favicon.ico
│   ├── globals.css
│   ├── layout.tsx
│   ├── muiSetup.tsx            // MUI theme setup
│   ├── page.tsx                // Home page (/)
│   ├── queryClient.tsx         // API client setup
│   ├── themeMode.tsx           // Theme mode (light/dark) setup
│   └── theme.tsx               // MUI theme definition
├── components                  // App components
└── lib                         // Utilities
    ├── api
    │   ├── constants.ts
    │   ├── index.ts
    │   └── types.ts
    └── utils.ts
```

## Stack

- NextJS (framework)
- Material UI (UI library)
- React Query (API client)
