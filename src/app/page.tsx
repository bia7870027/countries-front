"use client";

import Typography from "@mui/material/Typography";
import { useQuery } from "@tanstack/react-query";

import api from "../lib/api";
import API_CONSTANTS from "../lib/api/constants";
import { APICountry } from "../lib/api/types";

import Countries from "../components/Countries";
import StatusMessage from "../components/StatusMessage";

export default function Home() {
  const {
    data: countries,
    isLoading,
    isError,
  } = useQuery([API_CONSTANTS.countries.uniqueKey], api.countries.getAll());

  const ContentComponent = isLoading
    ? () => <StatusMessage variant="loading" />
    : isError
    ? () => <StatusMessage variant="error" />
    : () => <Countries countries={countries as unknown as APICountry[]} />;

  return <ContentComponent />;
}
