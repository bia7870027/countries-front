"use client";

import clsx from "clsx";
import { useQuery } from "@tanstack/react-query";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";

import api from "../../../lib/api";
import API_CONSTANTS from "../../../lib/api/constants";
import { APICountry } from "../../../lib/api/types";
import { getNativeNameFrom } from "../../../lib/utils";

import BackButton from "../../../components/BackButton";
import CountryBorders from "../../../components/CountryBorders";
import Main from "../../../components/Main";
import StatusMessage from "../../../components/StatusMessage";
import Wrapper from "../../../components/Wrapper";

import classes from "./page.module.css";

interface Params {
  slug: string;
}

interface CountryPageProps {
  params: Params;
}

const CountryPage = ({ params: { slug } }: CountryPageProps) => {
  const name = slug.replaceAll(/-/g, " ");

  const { data, isLoading, isError } = useQuery(
    [API_CONSTANTS.countries.uniqueKey, name],
    api.countries.get(name)
  );
  const country: APICountry | null = data && data.length > 0 ? data[0] : null;

  return (
    <>
      {isLoading && <StatusMessage variant="loading" />}
      {isError && <StatusMessage variant="error" />}

      <Main>
        {country && (
          <Wrapper>
            <BackButton
              className={clsx(classes.backButton, classes.override)}
            />

            <Grid container className={classes.content}>
              <Grid item xs={12} sm={6} md={5}>
                <img
                  src={country.flags.svg}
                  alt={country.name.common}
                  className={classes.flag}
                />
              </Grid>

              <Grid item xs={12} sm={6} md={7} className={classes.info}>
                <Typography
                  variant="h1"
                  className={clsx(classes.info__title, classes.override)}
                >
                  {country.name.common}
                </Typography>

                <Grid container className={classes.infoSections} spacing={4}>
                  <Grid
                    item
                    component="section"
                    xs={12}
                    md={6}
                    className={classes.infoSections__section}
                  >
                    <Typography variant="body2">
                      <strong>Native name: </strong>
                      {getNativeNameFrom(country.name.nativeName)}
                    </Typography>
                    <Typography variant="body2">
                      <strong>Population: </strong>
                      {country.population}
                    </Typography>
                    <Typography variant="body2">
                      <strong>Region: </strong>
                      {country.region}
                    </Typography>
                    <Typography variant="body2">
                      <strong>Sub Region: </strong>
                      {country.subregion}
                    </Typography>
                    <Typography variant="body2">
                      <strong>Capital: </strong>
                      {country.capital[0]}
                    </Typography>
                  </Grid>

                  <Grid
                    item
                    component="section"
                    xs={12}
                    md={6}
                    className={classes.infoSections__section}
                  >
                    <Typography variant="body2">
                      <strong>Top Level Domain: </strong>
                      {country.tld?.join(", ") ?? "-"}
                    </Typography>
                    <Typography variant="body2">
                      <strong>Currencies: </strong>
                      {Object.values(country.currencies)
                        .map(
                          (currency) => `${currency.name} (${currency.symbol})`
                        )
                        .join(", ")}
                    </Typography>
                    <Typography variant="body2">
                      <strong>Languages: </strong>
                      {Object.values(country.languages).join(", ")}
                    </Typography>
                  </Grid>
                </Grid>
                {country.borders && country.borders.length > 0 && (
                  <CountryBorders borders={country.borders} />
                )}
              </Grid>
            </Grid>
          </Wrapper>
        )}
      </Main>
    </>
  );
};

export default CountryPage;
