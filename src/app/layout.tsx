"use client";

import "./globals.css";

import { MuiSetup } from "./muiSetup";
import ThemeModeProvider from "./themeMode";
import QueryClientProvider from "./queryClient";

import Header from "../components/Header";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body>
        <QueryClientProvider>
          <ThemeModeProvider>
            <MuiSetup>
              <Header />
              {children}
            </MuiSetup>
          </ThemeModeProvider>
        </QueryClientProvider>
      </body>
    </html>
  );
}
