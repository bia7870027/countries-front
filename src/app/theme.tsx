import { ReactNode } from "react";
import { createTheme, ThemeProvider, Components } from "@mui/material/styles";

const COLORS = {
  darkBlue: "hsl(209, 23%, 22%)",
  veryDarkBlueBackground: "hsl(207, 26%, 17%)",
  veryDarkBlueText: "hsl(200, 15%, 8%)",
  darkGrayInput: "hsl(0, 0%, 52%)",
  veryLightGrayBackground: "hsl(0, 0%, 98%)",
  white: "hsl(0, 0%, 100%)",
};

const BOX_SHADOW = "0 0 8px rgba(0, 0, 0, 0.2)";

const typography = {
  fontFamily: "Nunito Sans",
  fontWeightRegular: 600,
  fontSize: 16,
  h1: {
    fontWeight: 800,
    fontSize: 24,
  },
  h2: {
    fontWeight: 800,
    fontSize: 20,
  },
  body1: {
    fontSize: 16,
  },
  body2: {
    marginBottom: 16,
  },
};

const components: Components = {
  MuiAppBar: {
    styleOverrides: {
      root: {
        boxShadow: BOX_SHADOW,
      },
    },
  },
  MuiButton: {
    styleOverrides: {
      root: {
        textTransform: "none",
        fontWeight: 600,
        lineHeight: 1,
      },
      contained: {
        boxShadow: BOX_SHADOW,
      },
    },
  },
  MuiCard: {
    styleOverrides: {
      root: {
        boxShadow: BOX_SHADOW,
      },
    },
  },
  MuiCardContent: {
    styleOverrides: {
      root: {
        paddingTop: 32,
        paddingLeft: 24,
        paddingRight: 24,
        paddingBottom: 32,
        "&:last-child": {
          paddingBottom: 48,
        },
      },
    },
  },
  MuiInputBase: {
    styleOverrides: {
      root: {
        "& fieldset": {
          borderWidth: 0,
        },
        "&:hover fieldset": {
          borderWidth: 1,
        },
      },
    },
  },
  MuiSelect: {
    styleOverrides: {
      select: {
        boxShadow: BOX_SHADOW,
      },
    },
  },
  MuiTextField: {
    styleOverrides: {
      root: {
        boxShadow: BOX_SHADOW,
        borderRadius: 4,
      },
    },
  },
};

const darkTheme = createTheme({
  palette: {
    primary: {
      main: COLORS.darkBlue,
    },
    background: {
      default: COLORS.veryDarkBlueBackground,
    },
    text: {
      primary: COLORS.white,
    },
  },
  typography: {
    ...typography,
  },
  components: {
    ...components,
    MuiMenu: {
      styleOverrides: {
        paper: {
          background: COLORS.darkBlue,
          color: COLORS.white,
        },
      },
    },
    MuiMenuItem: {
      styleOverrides: {
        root: {
          "&:hover": {
            background: COLORS.veryDarkBlueBackground,
          },
        },
      },
    },
    MuiSelect: {
      styleOverrides: {
        select: {
          background: COLORS.darkBlue,
          ...(components!.MuiSelect!.styleOverrides!.select as Record<
            string,
            any
          >),
        },
        icon: {
          color: COLORS.white,
        },
      },
    },
    MuiTextField: {
      styleOverrides: {
        root: {
          ...(components!.MuiTextField!.styleOverrides!.root as Record<
            string,
            any
          >),
          background: COLORS.darkBlue,
        },
      },
    },
    MuiTypography: {
      styleOverrides: {
        root: {
          color: COLORS.white,
        },
      },
    },
  },
});

const lightTheme = createTheme({
  palette: {
    primary: {
      main: COLORS.veryDarkBlueText,
    },
    background: {
      default: COLORS.veryLightGrayBackground,
    },
    text: {
      primary: COLORS.darkBlue,
    },
  },
  typography: {
    ...typography,
  },
  components: {
    ...components,
    MuiButton: {
      styleOverrides: {
        root: {
          ...(components!.MuiButton!.styleOverrides!.root as Record<
            string,
            any
          >),
          color: COLORS.darkBlue,
        },
        contained: {
          ...(components!.MuiButton!.styleOverrides!.contained as Record<
            string,
            any
          >),
          background: COLORS.white,
          "&:hover": {
            background: COLORS.veryLightGrayBackground,
          },
        },
      },
    },
    MuiSelect: {
      styleOverrides: {
        select: {
          ...(components!.MuiSelect!.styleOverrides!.select as Record<
            string,
            any
          >),
        },
      },
    },
    MuiTypography: {
      styleOverrides: {
        root: {
          color: COLORS.darkBlue,
        },
        body2: {
          color: COLORS.darkGrayInput,
        },
      },
    },
    MuiTextField: {
      styleOverrides: {
        root: {
          ...(components!.MuiTextField!.styleOverrides!.root as Record<
            string,
            any
          >),
          "& input": {
            color: `${COLORS.darkGrayInput}`,
          },
        },
      },
    },
  },
});

type ThemeMode = "light" | "dark";

interface ThemeProps {
  mode?: ThemeMode;
  children: ReactNode;
}

const Theme = ({ children, mode = "light" }: ThemeProps) => {
  const selectedTheme = mode === "dark" ? darkTheme : lightTheme;
  return <ThemeProvider theme={selectedTheme}>{children}</ThemeProvider>;
};

export default Theme;
