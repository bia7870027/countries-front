"use client";

import {
  createContext,
  useState,
  useEffect,
  Dispatch,
  ReactNode,
  SetStateAction,
} from "react";

export type ThemeMode = "light" | "dark";
export type ThemeToggleFunction = () => void;
export type ThemeModeContextValue = [ThemeMode, ThemeToggleFunction];

export const ThemeModeContext = createContext<
  ThemeModeContextValue | undefined
>(undefined);

const themeToggle =
  (setThemeMode: Dispatch<SetStateAction<ThemeMode>>) => () => {
    setThemeMode((prev) => {
      const newThemeMode = prev === "light" ? "dark" : "light";
      localStorage.setItem("themeMode", newThemeMode);
      return newThemeMode;
    });
  };

interface ThemeModeProviderProps {
  children: ReactNode;
}

const ThemeModeProvider = ({ children }: ThemeModeProviderProps) => {
  const [themeMode, setThemeMode] = useState<ThemeMode>(() => {
    const storedThemeMode = localStorage.getItem("themeMode");
    return storedThemeMode === "dark" ? "dark" : "light";
  });

  useEffect(() => {
    localStorage.setItem("themeMode", themeMode);
  }, [themeMode]);

  return (
    <ThemeModeContext.Provider value={[themeMode, themeToggle(setThemeMode)]}>
      {children}
    </ThemeModeContext.Provider>
  );
};

export default ThemeModeProvider;
