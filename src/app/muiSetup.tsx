"use client";

import { ReactNode, useContext } from "react";
import { NextAppDirEmotionCacheProvider } from "tss-react/next/appDir";

import ThemeProvider from "./theme";
import ThemeModeProvider, {
  ThemeModeContext,
  ThemeModeContextValue,
} from "./themeMode";

type Props = {
  children: ReactNode;
};

export const MuiSetup = ({ children }: Props) => {
  const [themeMode] = useContext(ThemeModeContext) as ThemeModeContextValue;
  return (
    <>
      <NextAppDirEmotionCacheProvider options={{ key: "css" }}>
        <ThemeProvider mode={themeMode}>{children}</ThemeProvider>
      </NextAppDirEmotionCacheProvider>
    </>
  );
};
