import { NativeNames } from "./api/types";

export const getNativeNameFrom = (nativeNames: NativeNames): string => {
  return Object.values(nativeNames)[0].official;
};
