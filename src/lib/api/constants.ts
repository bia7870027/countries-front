const API_URL = "https://restcountries.com/v3.1/";

const API_CONSTANTS = {
  countries: {
    mainEndpoint: `${API_URL}`,
    uniqueKey: "countries",
  },
};

export default API_CONSTANTS;
