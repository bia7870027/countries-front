import axios, { AxiosResponse } from "axios";

import API_CONSTANTS from "./constants";
import { APICountry } from "./types";

const api = {
  countries: {
    getAll: () => (): Promise<APICountry[]> =>
      axios
        .get(`${API_CONSTANTS.countries.mainEndpoint}all/`)
        .then(({ data }) => data),
    get: (id: string, byCode?: boolean) => (): Promise<APICountry[]> => {
      const idType = byCode ? "alpha/" : "name/";
      return axios
        .get(`${API_CONSTANTS.countries.mainEndpoint}${idType}${id}`)
        .then(({ data }) => data);
    },
  },
};

export default api;
