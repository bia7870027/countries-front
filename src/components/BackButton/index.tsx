import Button from "@mui/material/Button";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";

interface BackButtonProps {
  className?: string;
}

const BackButton = ({ className = "" }: BackButtonProps) => {
  const handleGoBack = () => {
    window.history.back();
  };

  return (
    <Button
      color="primary"
      onClick={handleGoBack}
      variant="contained"
      startIcon={<ArrowBackIcon />}
      className={className}
    >
      Back
    </Button>
  );
};

export default BackButton;
