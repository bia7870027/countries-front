import { ReactNode } from "react";

import classes from "./index.module.css";

interface WrapperProps {
  children: ReactNode;
}

const Wrapper = ({ children }: WrapperProps) => {
  return <div className={classes.wrapper}>{children}</div>;
};

export default Wrapper;
