import clsx from "clsx";
import { ReactNode, useContext } from "react";
import Box from "@mui/material/Box";
import { useTheme } from "@mui/material/styles";

import { ThemeModeContext, ThemeModeContextValue } from "../../app/themeMode";

import classes from "./index.module.css";

interface MainProps {
  children: ReactNode;
  center?: boolean;
}

const Main = ({ children, center }: MainProps) => {
  const [themeMode] = useContext(ThemeModeContext) as ThemeModeContextValue;
  const {
    palette: {
      background: { default: backgroundColor },
    },
  } = useTheme();

  return (
    <Box
      className={clsx(classes.main, center && classes["main--center"])}
      sx={{
        backgroundColor: themeMode === "dark" ? backgroundColor : undefined,
      }}
    >
      {children}
    </Box>
  );
};

export default Main;
