import clsx from "clsx";
import React, { useContext } from "react";
import { AppBar, Button, Toolbar, Typography } from "@mui/material";
import { Brightness4, Brightness7 } from "@mui/icons-material";
import Link from "next/link";

import { ThemeModeContext, ThemeModeContextValue } from "../../app/themeMode";

import classes from "./index.module.css";

const BUTTON_CONTENT = {
  light: {
    startIcon: <Brightness4 />,
    label: "Dark mode",
  },
  dark: {
    startIcon: <Brightness7 />,
    label: "Light mode",
  },
};

const Header = () => {
  const [themeMode, toggleThemeMode] = useContext(
    ThemeModeContext
  ) as ThemeModeContextValue;
  const { startIcon, label } =
    themeMode === "light" ? BUTTON_CONTENT.light : BUTTON_CONTENT.dark;

  return (
    <AppBar
      position="static"
      sx={{ backgroundColor: themeMode === "light" ? "white" : undefined }}
      className={classes.header}
    >
      <Toolbar>
        <Typography
          component="h1"
          variant="h1"
          sx={{ flexGrow: 1 }}
          className={clsx(classes.logotype, classes.override)}
        >
          <Link href="/" className={classes.logotypeLink}>
            Where in the world?
          </Link>
        </Typography>
        <Button color="inherit" onClick={toggleThemeMode} startIcon={startIcon}>
          {label}
        </Button>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
