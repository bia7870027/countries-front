import clsx from "clsx";
import { useContext } from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import Link from "next/link";
import { useTheme } from "@mui/material/styles";

import { APICountry } from "../../lib/api/types";
import { ThemeModeContext, ThemeModeContextValue } from "../../app/themeMode";

import classes from "./index.module.css";

interface CountryCardProps extends APICountry {}

const CountryCard = ({
  flags,
  name,
  population,
  region,
  capital,
}: CountryCardProps) => {
  const [themeMode] = useContext(ThemeModeContext) as ThemeModeContextValue;
  const {
    palette: {
      primary: { main: mainColor },
    },
  } = useTheme();

  return (
    <Card
      className={classes.card}
      sx={{ backgroundColor: themeMode === "dark" ? mainColor : undefined }}
    >
      <Link href={`/country/${name.common.replaceAll(/\s/g, "-")}`} passHref>
        <CardMedia
          sx={{ height: "0", paddingTop: "66.25%" }}
          image={flags.svg}
          title={name.common}
        />
      </Link>
      <CardContent>
        <Typography
          variant="h2"
          className={clsx(classes.card__title, classes.override)}
        >
          {name.common}
        </Typography>
        <Typography
          variant="body2"
          className={clsx(classes.card__text, classes.override)}
        >
          <strong>Population:</strong> {population.toLocaleString()}
        </Typography>
        <Typography
          variant="body2"
          className={clsx(classes.card__text, classes.override)}
        >
          <strong>Region:</strong> {region}
        </Typography>
        <Typography
          variant="body2"
          className={clsx(classes.card__text, classes.override)}
        >
          <strong>Capital:</strong> {capital}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default CountryCard;
