export const REGION_LABELS: Record<string, string> = {
  Africa: "Africa",
  Americas: "America",
  Asia: "Asia",
  Europe: "Europe",
  Oceania: "Oceania",
};
