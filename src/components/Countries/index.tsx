import clsx from "clsx";
import { useState, useContext, ChangeEvent } from "react";
import { SelectChangeEvent } from "@mui/material";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import { useTheme } from "@mui/material/styles";
import SearchIcon from "@mui/icons-material/Search";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import { useThrottle } from "react-use";

import { APICountry } from "../../lib/api/types";
import { ThemeModeContext, ThemeModeContextValue } from "../../app/themeMode";

import CountryCard from "../CountryCard";
import Wrapper from "../Wrapper";

import { REGION_LABELS } from "./constants";
import classes from "./index.module.css";

interface CountriesProps {
  countries: APICountry[];
}

const Countries = ({ countries }: CountriesProps) => {
  const [themeMode] = useContext(ThemeModeContext) as ThemeModeContextValue;
  const {
    palette: {
      background: { default: backgroundColor },
    },
  } = useTheme();

  const [searchValue, setSearchValue] = useState("");
  const [regionFilter, setRegionFilter] = useState("");

  const throttledSearchValue = useThrottle(searchValue, 100);

  const filteredCountries = countries
    .filter((country: APICountry) =>
      country.name.common
        .toLowerCase()
        .includes(throttledSearchValue.toLowerCase())
    )
    .filter((country: APICountry) =>
      regionFilter ? country.region === regionFilter : true
    );

  const handleSearchChange = (event: ChangeEvent<HTMLInputElement>) => {
    setSearchValue(event.target.value);
  };

  const handleRegionChange = (event: SelectChangeEvent) => {
    console.log(event);
    setRegionFilter(event.target.value);
  };

  const ContentComponent =
    filteredCountries.length > 0
      ? () => (
          <Grid container spacing={8} rowSpacing={8}>
            {filteredCountries.map((country, i) => (
              <Grid item xs={12} sm={6} md={4} lg={3} key={`country-${i}`}>
                <CountryCard {...country} />
              </Grid>
            ))}
          </Grid>
        )
      : () => <Typography>No countries match the query</Typography>;

  return (
    <Box
      component="main"
      className={classes.countries}
      sx={{
        backgroundColor: themeMode === "dark" ? backgroundColor : undefined,
      }}
    >
      <Wrapper>
        <div className={classes.filters}>
          <TextField
            className={clsx(classes.searchBox, classes.override)}
            placeholder="Search for a country..."
            variant="outlined"
            value={searchValue}
            onChange={handleSearchChange}
            fullWidth
            InputProps={{
              startAdornment: <SearchIcon className={classes.searchIcon} />,
            }}
          />

          <Select
            className={clsx(classes.dropdown, classes.override)}
            variant="outlined"
            displayEmpty
            renderValue={(value: string) =>
              value === "" ? "Filter by region" : REGION_LABELS[value]
            }
            value={regionFilter}
            onChange={handleRegionChange}
            fullWidth
            inputProps={{ "aria-label": "Filter by region" }}
          >
            <MenuItem value="">&nbsp;</MenuItem>
            {Object.entries(REGION_LABELS).map(([value, label], i) => {
              return (
                <MenuItem key={`menu-item-${i}`} value={value}>
                  {label}
                </MenuItem>
              );
            })}
          </Select>
        </div>

        <ContentComponent />
      </Wrapper>
    </Box>
  );
};

export default Countries;
