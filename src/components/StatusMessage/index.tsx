import clsx from "clsx";
import { useContext } from "react";
import LinearProgress from "@mui/material/LinearProgress";
import Typography from "@mui/material/Typography";
import ErrorOutlineIcon from "@mui/icons-material/ErrorOutline";
import { useTheme } from "@mui/material/styles";

import { ThemeModeContext, ThemeModeContextValue } from "../../app/themeMode";

import Main from "../Main";

import classes from "./index.module.css";

type StatusMessageVariant = "error" | "loading";

interface StatusMessageProps {
  variant: StatusMessageVariant;
}

const StatusMessage = ({ variant }: StatusMessageProps) => {
  const [themeMode] = useContext(ThemeModeContext) as ThemeModeContextValue;
  const {
    palette: {
      background: { default: backgroundColor },
    },
  } = useTheme();

  return (
    <Main center>
      <div className={classes.content}>
        {variant === "loading" ? (
          <>
            <Typography
              className={clsx(classes.content__text, classes.override)}
            >
              Loading...
            </Typography>
            <LinearProgress />
          </>
        ) : (
          <Typography>
            <ErrorOutlineIcon className={classes.content__icon} />
            <br />
            An error ocurred while loading the data
          </Typography>
        )}
      </div>
    </Main>
  );
};

export default StatusMessage;
