import clsx from "clsx";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Link from "next/link";
import { useQuery } from "@tanstack/react-query";

import api from "../../lib/api";
import API_CONSTANTS from "../../lib/api/constants";
import { APICountry } from "../../lib/api/types";

import classes from "./index.module.css";

interface BorderCountryButtonProps {
  countryCode: string;
}

const BorderCountryButton = ({ countryCode }: BorderCountryButtonProps) => {
  const { data } = useQuery(
    [API_CONSTANTS.countries.uniqueKey, countryCode],
    api.countries.get(countryCode, true)
  );
  const country: APICountry | null = data && data.length > 0 ? data[0] : null;

  return (
    country && (
      <Button
        variant="contained"
        className={clsx(classes.borderCountry, classes.override)}
        component={Link}
        href={`/country/${country.name.common.replaceAll(/\s/g, "-")}`}
      >
        {country.name.common}
      </Button>
    )
  );
};

interface CountryBordersProps {
  borders: string[];
}

const CountryBorders = ({ borders }: CountryBordersProps) => {
  return (
    <div className={classes.borderCountries}>
      <Typography
        variant="h2"
        className={clsx(classes.borderCountries__title, classes.override)}
      >
        Border Countries:
      </Typography>
      {borders.map((country, i) => (
        <BorderCountryButton countryCode={country} key={i} />
      ))}
    </div>
  );
};

export default CountryBorders;
